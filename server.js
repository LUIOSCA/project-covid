const express = require("express");
var axios = require('axios');
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
var nodemailer = require('nodemailer');

app.get("/coviddaily", async function (req, res) {
  res.header("Access-Control-Allow-Origin", "*");

    var response = await headerApi();
    var result = [];
    var covid = {result};

    for (var i in response.data) {
      var fecha =response.data[i].date
      //fecha.replace(/(\d{4})(\d{2})(\d{2})/g, '$1-$2-$3');
      var info={date:fecha,
                    positivos:response.data[i].positive,
                    negativos:response.data[i].negative,
                    pendiente:response.data[i].pending,
                    muertes:response.data[i].death}
      
      result.push(info);
    }
    res.send(covid);

   //res.send(SON.stringify(response.data))
   
});



async function headerApi() {

    let config = {
      method: "get",
      url: "https://api.covidtracking.com/v1/us/daily.json",
      headers: {},
    };
    var respuesta = "";
    try {
      const response = await axios(config);
      respuesta = JSON.stringify(response.data);
      return response;
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  app.listen(process.env.PORT || 3000,() => { 
    console.log("servidor montado en el puerto 3000");
  });

  app.post("/sendNotification", async function (req, res) {

    var destinatarios=req.destinatarios;
    var mensaje=req.mensaje;
    console.log(req.body);
    var transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'aquí va el correo desde donde se envia el mensaje',
        pass: 'aquí va la contraseña'
      }
    });
    var mailOptions = {
      from: 'aquí va el correo desde donde se envia el mensaje',
      to: req.body.destinatarios,
      subject: 'Prueba mensaje',
      text: req.body.mensaje
    };
    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });

  });


